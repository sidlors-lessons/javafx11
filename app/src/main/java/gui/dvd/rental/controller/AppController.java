package gui.dvd.rental.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.squareup.okhttp.Request;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import gui.dvd.rental.model.Record;
import gui.dvd.rental.service.client.ApiException;
import gui.dvd.rental.service.client.api.CatalogApi;
import gui.dvd.rental.service.model.Store;

public class AppController implements Initializable {

    @FXML
    private Accordion accordion;

    @FXML
    private GridPane gridPane;

    private final CatalogApi api = new CatalogApi();

    public void loadTableDataStaff() {
        // Crear una instancia del cliente REST y obtener los datos
        // RestClient client = new RestClient();
        // List<Record> records = client.getRecords();

        List<Record> records = new ArrayList<Record>();

        // Crear registros de ejemplo
        Record record1 = new Record("John", "Doe", 25);
        Record record2 = new Record("Jane", "Smith", 30);
        Record record3 = new Record("Mike", "Johnson", 28);

        // Agregar los registros a la lista
        records.add(record1);
        records.add(record2);
        records.add(record3);

        // Crear la tabla
        TableView<Record> tableView = new TableView<>();

        // Crear las columnas
        TableColumn<Record, String> column1 = new TableColumn<>("firstName");
        TableColumn<Record, String> column2 = new TableColumn<>("lastName");
        TableColumn<Record, String> column3 = new TableColumn<>("age");
        // ... Crear más columnas según tus necesidades

        // Asignar los valores de las propiedades de los registros a las celdas de las
        // columnas
        column1.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        column2.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        column3.setCellValueFactory(new PropertyValueFactory<>("age"));
        // ... Asignar más propiedades a las columnas

        // Agregar las columnas a la tabla
        tableView.getColumns().addAll(column1, column2,column3);

        // Agregar los datos a la tabla
        tableView.setItems(FXCollections.observableArrayList(records));

        // Agregar la tabla al GridPane
        gridPane.add(tableView, 0, 0);
    }


    public void loadTableDataStore() {
        // Crear una instancia del cliente REST y obtener los datos
        // RestClient client = new RestClient();
        // List<Record> records = client.getRecords();
        List<Store> records = new ArrayList<>();

        try {
           records  = api.getAllStores();

        } catch (ApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

       

        // Crear la tabla
        TableView<Store> tableView = new TableView<>();

        // Crear las columnas
        TableColumn<Store, String> column1 = new TableColumn<>("storeId");
        TableColumn<Store, String> column2 = new TableColumn<>("managerStaffId");
        TableColumn<Store, String> column3 = new TableColumn<>("addressId");
        TableColumn<Store, String> column4 = new TableColumn<>("lastUpdate");
        // ... Crear más columnas según tus necesidades

        // Asignar los valores de las propiedades de los registros a las celdas de las
        // columnas
        column1.setCellValueFactory(new PropertyValueFactory<>("storeId"));
        column2.setCellValueFactory(new PropertyValueFactory<>("managerStaffId"));
        column3.setCellValueFactory(new PropertyValueFactory<>("addressId"));
        column4.setCellValueFactory(new PropertyValueFactory<>("lastUpdate"));
        // ... Asignar más propiedades a las columnas

        // Agregar las columnas a la tabla
        tableView.getColumns().addAll(column1, column2,column3,column4);

        // Agregar los datos a la tabla
        tableView.setItems(FXCollections.observableArrayList(records));

        // Agregar la tabla al GridPane
        gridPane.add(tableView, 0, 0);
    }

    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    // Otros métodos y lógica de la ventana inicial

}